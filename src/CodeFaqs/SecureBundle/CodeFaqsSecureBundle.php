<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\SecureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CodeFaqsSecureBundle extends Bundle
{
}
