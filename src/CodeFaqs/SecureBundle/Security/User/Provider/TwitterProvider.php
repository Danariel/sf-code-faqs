<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\SecureBundle\Security\User\Provider;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use \TwitterOAuth;
use FOS\UserBundle\Doctrine\UserManager;

class TwitterProvider implements UserProviderInterface
{
    /** 
     * @var \Twitter
     */
    protected $twitterOAuth;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var Session
     */
    protected $session;


    /**
     * Construct method
     * 
     * @param TwitterOAuth $twitterOAuth
     * @param UserManager  $userManager
     * @param Session      $session
     */
    public function __construct(TwitterOAuth $twitterOAuth, UserManager $userManager, Session $session)
    {
        $this->twitterOAuth = $twitterOAuth;
        $this->userManager = $userManager;
        $this->session = $session;
    }   

    public function supportsClass($class)
    {   
        return $this->userManager->supportsClass($class);
    }   


    /**
     * Find User given a Twitter id
     * 
     * @param String $username Username
     * 
     * @return User
     */
    public function findUserByUsername($username)
    {
        return $this->userManager->findUserBy(array('username' => $username));
    }   


    /**
     * Given a username, load user
     * 
     * @param String $username Username
     * 
     * @return Mixed
     */
    public function loadUserByUsername($username)
    {
        $user = $this->findUserByUsername($username);

         $this->twitterOAuth->setOAuthToken( $this->session->get('access_token') , $this->session->get('access_token_secret'));

        try {
             $info = $this->twitterOAuth->get('account/verify_credentials');
        } catch (\Exception $e) {
             $info = null;
        }

        if (!empty($info)) {


            /**
             * Non existing user. Creating new document
             */
            if (empty($user)) {
                $user = $this->userManager->createUser();
                $user->createFromTwitter();
                $user->setEnabled(true);
                $user->setPassword('');
            }

            $username = $info->screen_name;
            $user   ->setTwitterID($info->id)
                    ->setTwitterUsername($username)
                    ->setUsername($username)
                    ->setEmail('')
                    ->setTwitterPhoto($info->profile_image_url_https)
                    ->setTwitterLocation($info->location)
                    ->setTwitterName($info->name);

            $this->userManager->updateUser($user);
        }

        if (empty($user)) {
            throw new UsernameNotFoundException('The user is not authenticated on twitter');
        }

        return $user;

    }

    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user)) || !$user->getTwitterID()) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getTwitterID());
    }
}