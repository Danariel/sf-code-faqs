<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\SecureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CodeFaqs\SecureBundle\Entity\Traits\TwitterUserTrait;
use CodeFaqs\CoreBundle\Entity\Question;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{

    use TwitterUserTrait;
    

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var ArrayCollection
     * 
     * User's questions
     * 
     * @ORM\OneToMany(targetEntity="\CodeFaqs\CoreBundle\Entity\Question", mappedBy="user")
     */
    protected $questions;


    /**
     * @var ArrayCollection
     * 
     * User's answers
     * 
     * @ORM\OneToMany(targetEntity="\CodeFaqs\CoreBundle\Entity\Answer", mappedBy="user")
     */
    protected $answers;

}
