<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\SecureBundle\Entity\Traits;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait
 */
Trait TwitterUserTrait
{
    
    /** 
     * @var string
     * 
     * @\Doctrine\ORM\Mapping\Column(name="twitter_id", type="string", length=255)
     */
    protected $twitterId;


    /** 
     * @var string
     * 
     * @\Doctrine\ORM\Mapping\Column(name="twitter_username", type="string", length=50)
     */
    protected $twitterUsername;


    /**
     * @var string
     * 
     * @\Doctrine\ORM\Mapping\Column(name="twitter_photo", type="string", length=255)
     */
    protected $twitterPhoto;


    /**
     * @var string
     * 
     * @\Doctrine\ORM\Mapping\Column(name="twitter_name", type="string", length=255)
     */
    protected $twitterName;


    /**
     * @var string
     * 
     * @\Doctrine\ORM\Mapping\Column(name="twitter_location", type="string", length=255)
     */
    protected $twitterLocation;



    /**
     * Method called when is created from twitter
     * 
     * @return Object self Object
     */
    public function createFromTwitter()
    {
        $this->salt = '';

        return $this;      
    }


    /**
     * Set twitterID
     *
     * @param string $twitterID
     * 
     * @return Object self Object
     */
    public function setTwitterID($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }


    /**
     * Get twitterID
     *
     * @return string 
     */
    public function getTwitterID()
    {
        return $this->twitterId;
    }


    /**
     * Set twitter_username
     *
     * @param string $twitterUsername
     * 
     * @return Object self Object
     */
    public function setTwitterUsername($twitterUsername)
    {
        $this->twitterUsername = $twitterUsername;

        return $this;
    }


    /**
     * Get twitter_username
     *
     * @return string 
     */
    public function getTwitterUsername()
    {
        return $this->twitterUsername;
    }


    /**
     * Set photo
     *
     * @param string $photo
     * 
     * @return Object self Object
     */
    public function setTwitterPhoto($twitterPhoto)
    {
        $this->twitterPhoto = $twitterPhoto;

        return $this;
    }


    /**
     * Get photo
     *
     * @return string $photo
     */
    public function getTwitterPhoto()
    {
        return $this->twitterPhoto;
    }


    /**
     * Set name
     *
     * @param string $name
     * 
     * @return Object self Object
     */
    public function setTwitterName($twitterName)
    {
        $this->twitterName = $twitterName;

        return $this;
    }


    /**
     * Get name
     *
     * @return string $name
     */
    public function getTwitterName()
    {
        return $this->twitterName;
    }


    /**
     * Set location
     *
     * @param string $location
     * 
     * @return Object self Object
     */
    public function setTwitterLocation($twitterLocation)
    {
        $this->twitterLocation = $twitterLocation;

        return $this;
    }


    /**
     * Get location
     *
     * @return string $location
     */
    public function getTwitterLocation()
    {
        return $this->twitterLocation;
    }
}