<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CodeFaqsCoreBundle extends Bundle
{
}
