<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity\Abstracts;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * @ORM\MappedSuperclass
 */
abstract class AbstractVote extends AbstractEntity
{

    use ORMBehaviors\Timestampable\Timestampable;


    /**
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\SecureBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

}
