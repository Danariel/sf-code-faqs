<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CodeFaqs\CoreBundle\Entity\Abstracts\AbstractEntity;
use CodeFaqs\SecureBundle\Entity\User;


/**
 * @ORM\Entity
 * @ORM\Table(name="answers")
 */
class Answer extends AbstractEntity
{

    /**
     * @var string
     * 
     * Question's content. This is the body of the entity
     * 
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank
     */
    protected $content;


    /**
     * @var Question
     * 
     * Answer question
     * 
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\CoreBundle\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $question;


    /**
     * @var ArrayCollection
     * 
     * Answer votes
     * 
     * @ORM\OneToMany(targetEntity="\CodeFaqs\CoreBundle\Entity\AnswerVote", mappedBy="answer")
     */
    protected $votes;


    /**
     * @var User
     * 
     * Comment's User
     * 
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\SecureBundle\Entity\User", inversedBy="answers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;




    /**
     * Setters and Getters
     */


    /**
     * Set Answer's content
     * 
     * @param string $content Content
     * 
     * @return Answer self Object
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }


    /**
     * Retrieves question's content
     * 
     * @return string Content
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Set Answer's question
     * 
     * @param Question $question Question
     * 
     * @return Answer self Object
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;

        return $this;
    }


    /**
     * Get Answer's question
     * 
     * @return Question Answer's question
     */
    public function getQuestion()
    {
        return $this->question;
    }


    /**
     * Set Answer's user
     * 
     * @param User $user User
     * 
     * @return Answer self Object
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Get Answer's user
     * 
     * @return User Answer's user
     */
    public function getUser()
    {
        return $this->user;
    }
}
