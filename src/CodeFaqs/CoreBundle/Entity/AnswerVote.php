<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity;

use CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="answer_votes")
 */
class AnswerVote extends AbstractVote
{

    /**
     * @var Answer
     * 
     * Vote answer
     * 
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\CoreBundle\Entity\Answer", inversedBy="votes")
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     */
    private $answer;
}
