<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CodeFaqs\CoreBundle\Entity\Abstracts\AbstractEntity;
use CodeFaqs\SecureBundle\Entity\User;
use CodeFaqs\CoreBundle\Entity\Traits\VoteableTrait;


/**
 * @ORM\Entity
 * @ORM\Table(name="questions")
 */
class Question extends AbstractEntity
{

    use VoteableTrait;


    /**
     * @var string
     * 
     * Question's title. Short title for question
     * 
     * @ORM\Column(name="title", type="string", length=255, unique=true, nullable=false)
     * @Assert\NotBlank
     */
    protected $title;


    /**
     * @var string
     * 
     * Question's content. This is the body of the entity
     * 
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank
     */
    protected $content;


    /**
     * @var ArrayCollection
     * 
     * Question answers
     * 
     * @ORM\OneToMany(targetEntity="\CodeFaqs\CoreBundle\Entity\Answer", mappedBy="question")
     */
    protected $answers;


    /**
     * @var ArrayCollection
     * 
     * Votes
     * 
     * @ORM\OneToMany(targetEntity="\CodeFaqs\CoreBundle\Entity\QuestionVote", mappedBy="question")
     */
    protected $votes;


    /**
     * @var User
     * 
     * Comment's User
     * 
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\SecureBundle\Entity\User", inversedBy="questions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;





    /**
     * Setters and Getters
     */


    /**
     * Set Question's title
     * 
     * @param string $title Title
     * 
     * @return Question self Object
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }


    /**
     * Retrieves question's title
     * 
     * @return string Title
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set Question's content
     * 
     * @param string $content Content
     * 
     * @return Question self Object
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }


    /**
     * Retrieves question's content
     * 
     * @return string Content
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Set Question's user
     * 
     * @param User $user User
     * 
     * @return Question self Object
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Get Question's user
     * 
     * @return User Question's user
     */
    public function getUser()
    {
        return $this->user;
    }
}
