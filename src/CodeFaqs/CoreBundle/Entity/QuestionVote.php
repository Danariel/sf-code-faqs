<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity;

use CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="question_votes")
 */
class QuestionVote extends AbstractVote
{

    /**
     * @var Question
     * 
     * Vote question
     * 
     * @ORM\ManyToOne(targetEntity="\CodeFaqs\CoreBundle\Entity\Question", inversedBy="votes")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;
}