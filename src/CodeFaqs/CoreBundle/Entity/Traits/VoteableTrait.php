<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\CoreBundle\Entity\Traits;


/**
 * Voteable Trait provides entities a simple way to be voted.
 * 
 * Each entity must define its relationship with votes
 * as just must be with an entity imlpementing AbstractVote
 * 
 * This trait provides methods
 */
trait VoteableTrait
{

    /**
     * Return all votes
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection Votes
     */
    public function getVotes()
    {
        return $this->votes;
    }


    /**
     * Set all votes
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $votes Votes to be set
     * 
     * @return Object self Object
     */
    public function setVotes(\Doctrine\Common\Collections\ArrayCollection $votes)
    {
        $this->votes = $votes;

        return $this;
    }


    /**
     * Add vote in collection
     * 
     * @param \CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote $vote Vote to be added
     * 
     * @return Object self Object
     */
    public function addVote(\CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote $vote)
    {
        $this->votes[] = $votes;

        return $this;
    }


    /**
     * remove vote from collection
     * 
     * @param \CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote $vote Vote to be removed
     * 
     * @return Object self Object
     */
    public function removeVote(\CodeFaqs\CoreBundle\Entity\Abstracts\AbstractVote $vote)
    {
        $this->votes->removeElement($vote);

        return $this;
    }


    /**
     * Count number of votes has object
     * 
     * @return integer Number of votes
     */
    public function countVotes()
    {
        return $this->votes->count();
    }

}
