<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CodeFaqsFrontBundle extends Bundle
{
}
