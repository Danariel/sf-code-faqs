<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Index controller
 * 
 * @Route("/")
 */
class IndexController extends Controller
{
    /**
     * Simple home page action
     * 
     * @return array Data needed by view
     * 
     * @Route("", name="index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
