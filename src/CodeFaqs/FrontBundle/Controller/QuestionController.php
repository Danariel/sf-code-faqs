<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\SecureParam;

use CodeFaqs\CoreBundle\Entity\Question;
use CodeFaqs\CoreBundle\Entity\Answer;
use CodeFaqs\FrontBundle\Form\Type\QuestionType;
use CodeFaqs\FrontBundle\Form\Type\AnswerType;

/**
 * This controller if the manager of all question routes.
 * 
 * * List - /questions
 * * New  - /question/new
 * * View - /question/1
 * * Edit - /question/1/edit
 * 
 * @Route("/question")
 */
class QuestionController extends Controller
{
    
    /**
     * List all questions
     * 
     * @Route("s", name="question_list")
     * @Template()
     * 
     * @return array Data needed by view
     */
    public function listAction()
    {
        $questions = $this
            ->getDoctrine()
            ->getRepository('CodeFaqsCoreBundle:Question')
            ->findAll();

        return array(

            'questions' =>  $questions,
        );
    }

    
    /**
     * New question page
     * 
     * @param Request $request Request object
     * 
     * @return Array|RedirectResponse If Question has been created succesfuly, a RedirectResponse is returned. Otherwise simple array with view is returned
     * 
     * @Route("/new", name="question_new")
     * @Template()
     */
    public function newAction(Request $request)
    {

        $questionForm = $this->createForm(
            new QuestionType(),
            new Question()
        );

        $questionForm->handleRequest($request);

        if ($questionForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $question = $questionForm->getData();
            $user = $this->get('security.context')->getToken()->getUser();

            $question->setUser($user);
            $entityManager->persist($question);
            $entityManager->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Question posted succesfuly');

            // creating the ACL
            $aclProvider = $this->get('security.acl.provider');
            $objectIdentity = ObjectIdentity::fromDomainObject($question);
            $acl = $aclProvider->createAcl($objectIdentity);

            // retrieving the security identity of the currently logged-in user
            $securityIdentity = UserSecurityIdentity::fromAccount($user);

            // grant owner access
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);

            return new RedirectResponse($this->generateUrl('question_view', array(

                'question_id'   =>  $question->getId(),
            )));
        }


        return array(
            'questionForm'  =>  $questionForm->createView()
        );
    }


    /**
     * View one specific question
     * 
     * @param Request  $request  Request object
     * @param Question $question Question object
     * 
     * @return array Data needed by view
     * 
     * @ParamConverter("question", class="CodeFaqsCoreBundle:Question", options={
     *      "id" = "question_id"
     * })
     * 
     * @Route("/{question_id}", name="question_view", requirements={
     *      "question_id" = "\d*"
     * })
     * @Template()
     */
    public function viewAction(Request $request, Question $question)
    {
        $securityContext = $this->get('security.context');
        $canEdit = (true === $securityContext->isGranted('EDIT', $question));
        $answer = new Answer();
        $answer->setQuestion($question);

        $answerForm = $this->createForm(
            new AnswerType(),
            $answer
        );

        $answerForm->handleRequest($request);

        if ($answerForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $answer = $answerForm->getData();
            $user = $this->get('security.context')->getToken()->getUser();

            $answer->setUser($user);
            $entityManager->persist($answer);
            $entityManager->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Answer posted succesfuly');

            // creating the ACL
            $aclProvider = $this->get('security.acl.provider');
            $objectIdentity = ObjectIdentity::fromDomainObject($answer);
            $acl = $aclProvider->createAcl($objectIdentity);

            // retrieving the security identity of the currently logged-in user
            $securityIdentity = UserSecurityIdentity::fromAccount($user);

            // grant owner access
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);


            /**
             * Creating new answer for user
             */
            $answer = new Answer();
            $answer->setQuestion($question);

            $answerForm = $this->createForm(
                new AnswerType(),
                $answer
            );
        }

        $answers = $this
            ->getDoctrine()
            ->getRepository('CodeFaqsCoreBundle:Answer')
            ->findByQuestion($question);

        return array(

            'question'  =>  $question,
            'answerForm'=>  $answerForm->createView(),
            'answers'   =>  $answers,
            'canedit'   =>  $canEdit,
        );
    }

    
    /**
     * Question edition page. Only allowed by qustomers with edit permissions over related question
     * 
     * @param Request  $request  Request object
     * @param Question $question Question object
     * 
     * @return array Data needed by view
     * 
     * @Route("/{question_id}/edit", name="question_edit")
     * @Template()
     * 
     * @ParamConverter("question", class="CodeFaqsCoreBundle:Question", options={
     *      "id" = "question_id"
     * })
     * @SecureParam(name="question", permissions="EDIT")
     */
    public function editAction(Request $request, Question $question)
    {

        $questionForm = $this->createForm(
            new QuestionType(),
            $question
        );

        if ($request->getMethod() == 'POST') {
            $questionForm->submit($request);

            if ($questionForm->isValid()) {

                $entityManager = $this->getDoctrine()->getManager();
                $question = $questionForm->getData();
                $entityManager->flush();

                return new RedirectResponse($this->generateUrl('question_view', array(

                    'question_id'   =>  $question->getId(),
                )));
            }
        }


        return array(
            'questionForm'  =>  $questionForm->createView()
        );
    }
}
