<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\SecureParam;

/**
 * This controller if the manager of all question answers
 * 
 * @Route("/question/{question_id}/answer")
 */
class AnswerController extends Controller
{
    
}