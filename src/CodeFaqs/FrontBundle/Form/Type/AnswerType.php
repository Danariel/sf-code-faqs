<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Answer type
 */
class AnswerType extends AbstractType
{

    /**
     * Builds answer formtype
     * 
     * @param FormBuilderInterface $builder Builder
     * @param array                $options Options injected
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea')
            ->add('save', 'submit')
            ->add('reset', 'reset');
    }


    /**
     * Defines form type name
     */
    public function getName()
    {
        return 'answer';
    }
}