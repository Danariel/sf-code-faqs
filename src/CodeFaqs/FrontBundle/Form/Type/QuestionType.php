<?php

/**
 * CodeFaqs 2013
 * 
 * @author Marc Morera
 * @author Miguel Angel Rios
 */

namespace CodeFaqs\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Question type
 */
class QuestionType extends AbstractType
{

    /**
     * Builds question formtype
     * 
     * @param FormBuilderInterface $builder Builder
     * @param array                $options Options injected
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text')
            ->add('content', 'textarea')
            ->add('save', 'submit')
            ->add('reset', 'reset');
    }


    /**
     * Defines form type name
     */
    public function getName()
    {
        return 'question';
    }
}